resource "aws_security_group" "auxischef" {
	name = "auxischef"
	vpc_id = "${aws_vpc.auxis-vpc.id}"

	# Allow inbound for ssh
	ingress {
		from_port = 22
		to_port = 22
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}

	# Allow ICMP Requests
	ingress {
		from_port = -1
		to_port = -1
		protocol = "icmp"
		cidr_blocks = ["0.0.0.0/0"]
	}

	# Allow all outbound
	egress {
		from_port = 0
		to_port = 0
		protocol = -1
		cidr_blocks = ["0.0.0.0/0"]
	}
}
