resource "aws_instance" "auxischef-instance" {
	ami = "${var.ami_id}"
	instance_type = "${var.instance_type}"
	key_name = "${var.key_name}"
	subnet_id = "${aws_subnet.private-subnet.id}"
	vpc_security_group_ids = ["${aws_security_group.auxischef.id}"]
	source_dest_check = false
	private_ip = "${var.ip_chef}"
	tags {
		Name = "${var.chef_hostname}"
		Provider = "Terraform"
	}
}

resource "aws_eip" "auxischef-eip-instance" {
	instance = "${aws_instance.auxischef-instance.id}"
	vpc = true

	provisioner "local-exec" {
		command = "sleep 30"
	}

	provisioner "file" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxischef-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		source = "${var.key_file}"
		destination = "/home/${var.remote_user}/.ssh/id_rsa"
	}

	provisioner "file" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxischef-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		source = "${var.gitlab_key_file}"
		destination = "/home/${var.remote_user}/.ssh/gitlab_id_rsa"
	}

	provisioner "file" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxischef-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		source = "ssh_config"
		destination = "/home/${var.remote_user}/.ssh/config"
	}

	provisioner "remote-exec" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxischef-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		inline = [
			"sudo chown ${var.remote_user}:${var.remote_user} /home/${var.remote_user}/.ssh/id_rsa",
			"sudo chown ${var.remote_user}:${var.remote_user} /home/${var.remote_user}/.ssh/gitlab_id_rsa",
			"sudo chown ${var.remote_user}:${var.remote_user} /home/${var.remote_user}/.ssh/config",
			"sudo chmod 0400 /home/${var.remote_user}/.ssh/id_rsa",
			"sudo chmod 0400 /home/${var.remote_user}/.ssh/gitlab_id_rsa",
			"sudo chmod 0600 /home/${var.remote_user}/.ssh/config",
		]
	}

	provisioner "remote-exec" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxischef-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}

		inline = [
			"free -h",
			"sudo fallocate -l 1G /swapfile",
			"sudo chmod 600 /swapfile",
			"sudo mkswap /swapfile",
			"sudo swapon /swapfile",
			"sudo swapon --show",
			"free -h",
		]
	}

	provisioner "remote-exec" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxischef-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		inline = [
			"sudo sed -i 's/^\\(127.0.0.1.*localhost\\)$/\\1 ${var.chef_hostname}\\n${var.ip_lb}\\t${var.lb_hostname}\\n${var.ip_ws}\\t${var.ws_hostname}/' /etc/hosts",
			"echo ${var.chef_hostname} | sudo tee /etc/hostname",
			"sudo hostname ${var.chef_hostname}",
			"sudo apt-get update",
			"sudo apt-get install --no-install-recommends -y bash-completion ruby-dev make git build-essential git vim curl unzip vim tree wget s3cmd",
			"wget https://packages.chef.io/files/stable/chefdk/3.0.36/debian/9/chefdk_3.0.36-1_amd64.deb",
			"wget https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip",
			"sudo gem install kitchen-terraform bundler",
			"unzip terraform_0.11.7_linux_amd64.zip",
			"sudo mv terraform /usr/bin",
			"sudo dpkg -i chefdk_3.0.36-1_amd64.deb",
			"rm chefdk_3.0.36-1_amd64.deb terraform_0.11.7_linux_amd64.zip",
			"git config --global user.name '${var.user_name}'",
			"git config --global user.email '${var.user_email}'",
			"mkdir -p chef-repo",
			"cd chef-repo/",
			"ssh-keyscan gitlab.com >> ${var.user_known_hosts}",
			"git clone git@gitlab.com:mvicha/MarcosFVilla.git cookbooks",
		]
	}

	provisioner "file" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxischef-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		source = "auxis.tfvars"
		destination = "/home/${var.remote_user}/chef-repo/cookbooks/nginx/infrastructure/auxis.tfvars"
	}

	provisioner "remote-exec" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxischef-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		inline = [
			"cd chef-repo/cookbooks/nginx",
			"sudo bunlder install",
			"kitchen list",
			"kitchen create",
			"kitchen converge",
		]
	}
}

