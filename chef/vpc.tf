resource "aws_vpc" "auxis-vpc" {
	cidr_block = "${var.vpc_cidr}"
	enable_dns_support = true
	enable_dns_hostnames = true
	tags = {
		Name = "Auxis VPC"
	}
}

resource "aws_subnet" "public-subnet" {
	vpc_id = "${aws_vpc.auxis-vpc.id}"
	cidr_block = "${var.public_subnet_cidr}"
	map_public_ip_on_launch = true
	availability_zone = "${var.availability_zone}"
	tags = {
		Name =  "Subnet public Virginia 1a"
	}
}

resource "aws_subnet" "private-subnet" {
	vpc_id = "${aws_vpc.auxis-vpc.id}"
	cidr_block = "${var.private_subnet_cidr}"
	availability_zone = "${var.availability_zone}"
	tags = {
		Name = "Subnet private Virginia 1a"
	}
}

resource "aws_internet_gateway" "auxis-gw" {
	vpc_id = "${aws_vpc.auxis-vpc.id}"
	tags {
		Name = "InternetGateway"
	}
}

resource "aws_route" "internet-access" {
	route_table_id = "${aws_vpc.auxis-vpc.main_route_table_id}"
	destination_cidr_block = "0.0.0.0/0"
	gateway_id = "${aws_internet_gateway.auxis-gw.id}"
}

resource "aws_route" "private-route" {
	route_table_id  = "${aws_vpc.auxis-vpc.main_route_table_id}"
	destination_cidr_block = "0.0.0.0/0"
	gateway_id = "${aws_internet_gateway.auxis-gw.id}"
}

resource "aws_route_table_association" "public-subnet-association" {
	subnet_id = "${aws_subnet.public-subnet.id}"
	route_table_id = "${aws_vpc.auxis-vpc.main_route_table_id}"
}

resource "aws_route_table_association" "private-subnet-association" {
	subnet_id = "${aws_subnet.private-subnet.id}"
	route_table_id = "${aws_vpc.auxis-vpc.main_route_table_id}"
}

resource "local_file" "nginx_data_file" {
	content = <<EOF
access_key = "${var.access_key}"
secret_key = "${var.secret_key}"
auxis_vpc_id = "${aws_vpc.auxis-vpc.id}"
public_subnet_id = "${aws_subnet.public-subnet.id}"
private_subnet_id = "${aws_subnet.private-subnet.id}"
EOF
	filename = "auxis.tfvars"
}

