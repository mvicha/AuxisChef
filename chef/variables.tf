variable "user_known_hosts" {
	description = "Needed to store public key of the servers"
	default = "~/.ssh/known_hosts"
}

variable "state_bucket" {
	description = "Bucket name to store terraform state"
	default = "auxis-terraform-state"
}

variable "region" {
	description = "Region where the instance is going to be deployed"
	default = "us-east-1"
} 

variable "availability_zone" {
	description = "In which zone to deploy instance"
	default = "us-east-1a"
}

variable "instance_type" {
	description = "Type of instance to use"
	default = "t2.micro"
}

variable "instance_name" {
	description = "Name of the instance to deploy"
	default = "auxis-instance"
}

variable "ami_id" {
	description = "id of the AMI to use as base image"
	default = "ami-8c9bdaf3"
}

variable "remote_user" {
	description = "Remote user"
	default = "admin"
}

variable "user_name" {
	description = "GIT user.name"
	default = "Marcos F. Villa"
}

variable "user_email" {
	description = "GIT user.email"
	default = "mvicha@gmail.com"
}

variable "chef_repo" {
	description = "Chef repository"
	default = "git@gitlab.com:mvicha/MarcosFVilla.git"
}

variable "key_name" {
	description = "AWS key name"
	default = "glhygieia"
}

variable "key_file" {
	description = "Private Key File"
	default = "ssh/id_rsa"
}

variable "gitlab_key_file" {
	description = "Gitlab Private Key File"
	default = "ssh/gitlab_id_rsa"
}

variable "vpc_cidr" {
	description = "CIDR for the whole VPC"
	default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
	description = "CIDR for the Public Subnet"
	default = "10.0.0.0/24"
}

variable "private_subnet_cidr" {
	description = "CIDR for the Private Subnet"
	default = "10.0.1.0/24"
}

variable "ip_chef" {
	description = "Priave ip for Chef instance"
	default = "10.0.1.102"
}

variable "ip_ws" {
	description = "Priave ip for WS instance"
	default = "10.0.1.103"
}

variable "ip_lb" {
	description = "Priave ip for LB instance"
	default = "10.0.0.102"
}

variable "chef_hostname" {
	description = "Auxis Chef instance hostname"
	default = "auxischef"
}

variable "lb_hostname" {
	description = "Auxis lb instance hostname"
	default = "auxislb"
}

variable "ws_hostname" {
	description = "Auxis WS instance hostname"
	default = "auxisws"
}

variable "access_key" {}

variable "secret_key" {}
