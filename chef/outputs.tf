output "Auxis_Chef_id" {
	description = "List of Auxis Chef instance ID"
	value = ["${aws_instance.auxischef-instance.id}"]
}

output "Auxis_Chef_availability_zone" {
	description = "List of availability zones of Chef instance"
	value = ["${aws_instance.auxischef-instance.availability_zone}"]
}

output "Auxis_Chef_key_name" {
	description = "List of Chef key names"
	value = ["${aws_instance.auxischef-instance.key_name}"]
}

output "Auxis_Chef_public_dns" {
	description = "List of public DNS names assigned to the instances. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
	value = ["${aws_instance.auxischef-instance.public_dns}"]
}

output "Auxis_Chef_network_interface_id" {
	description = "List of IDs of the network interface of Chef instance"
	value = ["${aws_instance.auxischef-instance.network_interface_id}"]
}

output "Auxis_Chef_private_dns" {
	description = "List of private DNS names assigned to the LB instance. Can only be used inside the Amazon EC2, and only available if you've enabled DNS hostnames for your VPC"
	value = ["${aws_instance.auxischef-instance.private_dns}"]
}

output "Auxis_Chef_private_ip" {
	description = "List of private IP addresses assigned to the Chef instance"
	value = ["${aws_instance.auxischef-instance.private_ip}"]
}

output "Auxis_Chef_vpc_security_group_ids" {
	description = "List of associated security groups of Chef instance, if running in non-default VPC"
	value = ["${aws_instance.auxischef-instance.vpc_security_group_ids}"]
}

output "Auxis_Chef_subnet_id" {
	description = "List of IDs of VPC subnets of Chef instance"
	value = ["${aws_instance.auxischef-instance.subnet_id}"]
}

output "Auxis_Chef_elastic_ip" {
	description = "Elastic IP assigned to Auxis Chef Server"
	value = ["${aws_eip.auxischef-eip-instance.public_ip}"]
}

output "Auxis_Public_subnet_id" {
	description = "Public subnet id"
	value = ["${aws_subnet.public-subnet.id}"]
}


output "Auxis_Private_subnet_id" {
	description = "Private subnet id"
	value = ["${aws_subnet.private-subnet.id}"]
}

output "Auxis_VPC_id" {
	description = "VPC id"
	value = ["${aws_vpc.auxis-vpc.id}"]
}

