terraform {
	backend "s3" {
		encrypt = true
		bucket = "auxis-terraform-state"
		region = "us-east-1"
		key = "auxis-tfstate.tfstate"
	}
}

