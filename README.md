# AWS DevOps engineer evaluation

## Objective

Evaluate your knowledge/experience in three key areas: AWS, Chef and Terraform.

## Asumptions

We assume that you have a working AWS account where you can test the infrastructure you're developing. If you don't have a personal account, you can create one and make use of the [AWS free tier](https://aws.amazon.com/free/).

## Topics

- AWS.
- Chef.
- Infrastructure as code (terraform).

## Details

### The Challenge

NGINX can act as regular web server or as a reverse proxy (load balancer). The challenge consists of creating two VPCs (VPC1 and VPC2)
each VPC has only one subnet, VPC 1 has a public subnet, and VPC2 has a private subnet. On VPC2 there is an EC2 instance, running
NGINX (let's call this the backend server); on VPC1 there's another EC2 instance running NGINX and acting as a reverse proxy
(or load balancer).

When an user hits the public IP address of the NGINX load balancer, it sends the traffic to the backend server, the backend server
responds and the website is delivered to the user and rendered in a web browser.

To do that, you have to create a (wrapper) Chef cookbook, and then use it from Terraform.

### Chef

Create a chef (wrapper) cookbook to install and configure NGINX and its requirements (you can use any comunity cookbooks - only from the Chef supermarket, or create your own cookbook from scratch). You must also include tests for your cookbook (using test kitchen +
serverspec). Additionally, your cookbook must pass food critic and rubocop.

### Terraform

#### AWS Resources

You'll need to create (at least) the following resources:
- Two VPCs.
- One subnet for each VPC.
- At least 2 security groups.
- Two ec2 instances.

The following diagram can give you a better idea of what you should code in terraform:

```
   +--|++++++++++|-----------------------------------------------------------------------------------+
   |  |  AWS     |                                                                                   |
   |  +----------+                                                                                   |
   |                                                                                                 |
   |   +--|+++++|-------------------------------+       +--|+++++|-------------------------------+   |
   |   |  |VPC1 |                               |       |  |VPC2 |                               |   |
   |   |  +-----+                               |       |  +-----+                               |   |
   |   |                                        |       |                                        |   |
   |   |                                        |       |                                        |   |
   |   |   +|+++++++++++++++++++++++++++++++|+  |       |   +|+++++++++++++++++++++++++++++++|+  |   |
   |   |   || public subnet                 ||  |       |   || private subnet                ||  |   |
   |   |   |+-------------------------------+|  |       |   |+-------------------------------+|  |   |
   |   |   |  +---------------------------+  |  |       |   |  +---------------------------+  |  |   |
   |   |   |  |                           |  |  |       |   |  |                           |  |  |   |
   |   |   |  |  ec2 instance acting as   |<-+--+-------+---+->|  ec2 instance running a   |  |  |   |
   |   |   |  |  a load balancer (nginx)  |  |  |       |   |  |  web server(nginx)        |  |  |   |
   |   |   |  |                           |  |  |       |   |  |                           |  |  |   |
   |   |   |  +---------------------------+  |  |       |   |  +---------------------------+  |  |   |
   |   |   +---------------------------------+  |       |   +---------------------------------+  |   |
   |   |                                        |       |                                        |   |
   |   +----------------------------------------+       +----------------------------------------+   |
   +-------------------------------------------------------------------------------------------------+

```

## Deliverables
```
   +--|++++++++++|-----------------------------------------------------------------------------------+
   |  |  AWS     |                                                                                   |
   |  +----------+                                                                                   |
   |                                                                                                 |
   |                             +--|+++++|-------------------------------+                          |
   |                             |  |VPC2 |                               |                          |
   |                             |  +-----+                               |                          |
   |                             |   Instance name: auxischef             |                          |
   |                             |                                        |                          |
   |                             |   +|+++++++++++++++++++++++++++++++|+  |                          |
   |                             |   || private subnet                ||  |                          |
   |                             |   |+-------------------------------+|  |                          |
   |                             |   |  +---------------------------+  |  |                          |
   |                             |   |  | ec2 workstation prepared  |  |  |                          |
   |                             |   |  | with all necessary tools  |  |  |                          |
   |                             |   |  | to launch instances and   |  |  |                          |
   |                             |   |  | deploy nginx as requested |  |  |                          |
   |                             |   |  +---------------------------+  |  |                          |
   |                             |   +---------------------------------+  |                          |
   |                             |                                        |                          |
   |                             +------------------+---------------------+                          |
   |                                               / \                                               |
   |                                              /   \                                              |
   |   +--|+++++|-------------------------------+       +--|+++++|-------------------------------+   |
   |   |  |VPC1 |                               |       |  |VPC2 |                               |   |
   |   |  +-----+                               |       |  +-----+                               |   |
   |   |   Instance name: auxislb               |       |  Instance name: auxisws                |   |
   |   |                                        |       |                                        |   |
   |   |   +|+++++++++++++++++++++++++++++++|+  |       |   +|+++++++++++++++++++++++++++++++|+  |   |
   |   |   || public subnet                 ||  |       |   || private subnet                ||  |   |
   |   |   |+-------------------------------+|  |       |   |+-------------------------------+|  |   |
   |   |   |  +---------------------------+  |  |       |   |  +---------------------------+  |  |   |
   |   |   |  |                           |  |  |       |   |  |                           |  |  |   |
   |   |   |  |  ec2 instance acting as   |<-+--+-------+---+->|  ec2 instance running a   |  |  |   |
   |   |   |  |  a load balancer (nginx)  |  |  |       |   |  |  web server(nginx)        |  |  |   |
   |   |   |  |                           |  |  |       |   |  |                           |  |  |   |
   |   |   |  +---------------------------+  |  |       |   |  +---------------------------+  |  |   |
   |   |   +---------------------------------+  |       |   +---------------------------------+  |   |
   |   |                                        |       |                                        |   |
   |   +----------------------------------------+       +----------------------------------------+   |
   +-------------------------------------------------------------------------------------------------+

```

### Instructions

Requirements:
You only need to download Terraform for your current OS from https://www.terraform.io/downloads.html. This project provides the rest of the tools to achieve its objective.

Following are the steps to make this happen:

1) Obtain the repository:
  $ git clone git@gitlab.com:auxis/MarcosFVilla.git

2) Write the values of the provided AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY into MarcosFVilla/chef/terraform.tfvars

3) Create the environment variables, needed for the service to run the first time.
  $ export AWS_ACCESS_KEY_ID=""
  $ export AWS_SECRET_ACCESS_KEY=""

4) Copy the public key file you use for cloning the git repository into MarcosFVilla/chef/ssh/gitlab_id_rsa

5) Create a directory for ssh keys
  $ mkdir MarcosFVilla/chef/ssh

6) Copy the id_rsa file you received by email into MarcosFVilla/chef/ssh/id_rsa

7) Make sure the key files have the proper permissions
  $ chmod 400 MarcosFVilla/chef/ssh/gitlab_id_rsa
  $ chmod 400 MarcosFVilla/chef/ssh/id_rsa

8) Initialize Terraform and launch project
  $ cd MarcosFVilla/chef
  $ terraform init
  $ terraform apply
  Answer yes when asked

9) Wait for the process to finish. It takes around 12 minutes to complete

10) Open the URL shown in the output. You should get a webpage saying it is running on auxisws.

#### Example of WEB URL STRING TO LOOK FOR IN THE OUTPUT ####
aws_eip.auxischef-eip-instance (remote-exec):            "WEB URL": {
aws_eip.auxischef-eip-instance (remote-exec):         "sensitive": false,
aws_eip.auxischef-eip-instance (remote-exec):         "type": "string",
aws_eip.auxischef-eip-instance (remote-exec):         "value": "http://54.88.235.113/"
aws_eip.auxischef-eip-instance (remote-exec):            }
#############################################################


#### ROLLBACK ####
In case you want to roll back all the changes you will have to:

1) Obtain auxischef eip address:
  $ cd MarcosFVilla/chef
  $ terraform output
  Look for the value of Auxis_Chef_elastic_ip

2) SSH into auxischef
  $ ssh -i ssh/id_rsa admin@EIP

3) Inside auxischef run the following:
  $ cd chef-repo/nginx
  $ kitchen destroy

4) Logout of auxischef and destroy it
  $ terraform destroy
  Answer yes when asked
  In case you get any errors when destroying please try again

### Code

You have to ship your code to us, please create a pull request against the upstream repo.


admin@auxischef:~/chef-repo/cookbooks$ cookstyle chef/
Inspecting 0 files


0 files inspected, no offenses detected
admin@auxischef:~/chef-repo/cookbooks$ cookstyle nginx/
Inspecting 7 files
.......

7 files inspected, no offenses detected
admin@auxischef:~/chef-repo/cookbooks$ foodcritic chef/
Checking 0 files


admin@auxischef:~/chef-repo/cookbooks$ foodcritic nginx/
Checking 4 files
....

admin@auxischef:~/chef-repo/cookbooks$ 

