#
# Cookbook:: nginx
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

package 'nginx'

service 'nginx' do
  supports [status: true, restart: true, reload: true]
  action [:enable, :start]
end

template '/etc/nginx/sites-available/default' do
  source 'defaultsite.erb'
  notifies :restart, 'service[nginx]', :immediately
end

template '/var/www/html/index.nginx-debian.html' do
  source 'index.html.erb'
end
