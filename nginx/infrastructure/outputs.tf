output "Auxis_LB_id" {
	description = "List of Auxis LB instance ID"
	value = ["${aws_instance.auxislb-instance.id}"]
}

output "Auxis_WS_id" {
	description = "List of Auxis WS instance ID"
	value = ["${aws_instance.auxisws-instance.id}"]
}

output "Auxis_LB_availability_zone" {
	description = "List of availability zones of LB instance"
	value = ["${aws_instance.auxislb-instance.availability_zone}"]
}

output "Auxis_WS_availability_zone" {
	description = "List of availability zones of LB instance"
	value = ["${aws_instance.auxislb-instance.availability_zone}"]
}

output "Auxis_LB_key_name" {
	description = "List of WS key names"
	value = ["${aws_instance.auxisws-instance.key_name}"]
}

output "Auxis_WS_key_name" {
	description = "List of WS key names"
	value = ["${aws_instance.auxisws-instance.key_name}"]
}

output "Auxis_LB_public_dns" {
	description = "List of public DNS names assigned to the instances. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
	value = ["${aws_instance.auxislb-instance.public_dns}"]
}

output "Auxis_LB_network_interface_id" {
	description = "List of IDs of the network interface of LB instance"
	value = ["${aws_instance.auxislb-instance.network_interface_id}"]
}

output "Auxis_WS_network_interface_id" {
	description = "List of IDs of the network interface of WS instance"
	value = ["${aws_instance.auxisws-instance.network_interface_id}"]
}

output "Auxis_LB_private_dns" {
	description = "List of private DNS names assigned to the LB instance. Can only be used inside the Amazon EC2, and only available if you've enabled DNS hostnames for your VPC"
	value = ["${aws_instance.auxislb-instance.private_dns}"]
}

output "Auxis_WS_private_dns" {
	description = "List of private DNS names assigned to the WS instance. Can only be used inside the Amazon EC2, and only available if you've enabled DNS hostnames for your VPC"
	value = ["${aws_instance.auxisws-instance.private_dns}"]
}

output "Auxis_LB_private_ip" {
	description = "List of private IP addresses assigned to the LB instance"
	value = ["${aws_instance.auxislb-instance.private_ip}"]
}

output "Auxis_WS_private_ip" {
	description = "List of private IP addresses assigned to the WS instance"
	value = ["${aws_instance.auxisws-instance.private_ip}"]
}

output "Auxis_LB_vpc_security_group_ids" {
	description = "List of associated security groups of LB instance, if running in non-default VPC"
	value = ["${aws_instance.auxislb-instance.vpc_security_group_ids}"]
}

output "Auxis_WS_vpc_security_group_ids" {
	description = "List of associated security groups of WS instance, if running in non-default VPC"
	value = ["${aws_instance.auxisws-instance.vpc_security_group_ids}"]
}

output "Auxis_LB_subnet_id" {
	description = "List of IDs of VPC subnets of LB instance"
	value = ["${aws_instance.auxislb-instance.subnet_id}"]
}

output "Auxis_WS_subnet_id" {
	description = "List of IDs of VPC subnets of WS instance"
	value = ["${aws_instance.auxisws-instance.subnet_id}"]
}

output "Auxis_LB_elastic_ip" {
	description = "Elastic IP assigned to Auxis LB Server"
	value = ["${aws_eip.auxislb-eip-instance.public_ip}"]
}

output "WEB URL" {
	description = "Open This URL to test it's working"
	value = "http://${aws_eip.auxislb-eip-instance.public_ip}/"
}
