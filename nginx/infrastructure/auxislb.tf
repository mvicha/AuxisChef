resource "aws_instance" "auxislb-instance" {
	ami = "${var.ami_id}"
	instance_type = "${var.instance_type}"
	key_name = "${var.key_name}"
	subnet_id = "${var.public_subnet_id}"
	vpc_security_group_ids = ["${aws_security_group.auxislb.id}"]
	source_dest_check = false
	depends_on = ["aws_instance.auxisws-instance"]
	private_ip = "${var.ip_lb}"
	tags {
		Name = "${var.lb_hostname}"
		Provider = "Terraform"
	}

	provisioner "local-exec" {
		command = "ssh-keygen -f /home/admin/.ssh/known_hosts -R ${aws_instance.auxislb-instance.private_ip}"
	}
}

resource "aws_eip" "auxislb-eip-instance" {
	instance = "${aws_instance.auxislb-instance.id}"
	vpc = true

	provisioner "local-exec" {
		command = "sleep 30"
	}

	provisioner "file" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxislb-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		source = "${var.key_file}"
		destination = "/home/${var.remote_user}/.ssh/id_rsa"
	}

	provisioner "file" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxislb-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		source = "${var.gitlab_key_file}"
		destination = "/home/${var.remote_user}/.ssh/gitlab_id_rsa"
	}

	provisioner "file" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxislb-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		source = "infrastructure/ssh_config"
		destination = "/home/${var.remote_user}/.ssh/config"
	}

	provisioner "remote-exec" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxislb-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		inline = [
			"sudo chown ${var.remote_user}:${var.remote_user} /home/${var.remote_user}/.ssh/id_rsa",
			"sudo chown ${var.remote_user}:${var.remote_user} /home/${var.remote_user}/.ssh/gitlab_id_rsa",
			"sudo chown ${var.remote_user}:${var.remote_user} /home/${var.remote_user}/.ssh/config",
			"sudo chmod 0400 /home/${var.remote_user}/.ssh/id_rsa",
			"sudo chmod 0400 /home/${var.remote_user}/.ssh/gitlab_id_rsa",
			"sudo chmod 0600 /home/${var.remote_user}/.ssh/config",
		]
	}

	provisioner "remote-exec" {
		connection {
			type = "ssh"
			host = "${aws_eip.auxislb-eip-instance.public_ip}"
			agent = false
			user = "${var.remote_user}"
			private_key = "${file("${var.key_file}")}"
		}
		inline = [
			"sudo sed -i 's/^\\(127.0.0.1.*localhost\\)$/\\1 ${var.lb_hostname}\\n${var.ip_chef}\\t${var.chef_hostname}\\n${var.ip_ws}\\t${var.ws_hostname}/' /etc/hosts",
			"echo ${var.lb_hostname} | sudo tee /etc/hostname",
			"sudo hostname ${var.lb_hostname}",
			"sudo apt-get update",
			"sudo apt-get install --no-install-recommends -y bash-completion ruby-dev make git build-essential git vim curl unzip vim tree wget s3cmd",
			"wget https://packages.chef.io/files/stable/chefdk/3.0.36/debian/9/chefdk_3.0.36-1_amd64.deb",
			"sudo dpkg -i chefdk_3.0.36-1_amd64.deb",
			"rm chefdk_3.0.36-1_amd64.deb terraform_0.11.7_linux_amd64.zip",
			"git config --global user.name '${var.user_name}'",
			"git config --global user.email '${var.user_email}'",
			"mkdir -p chef-repo/cookbooks",
			"cd chef-repo/",
			"ssh-keyscan gitlab.com >> ${var.user_known_hosts}",
			"git clone git@gitlab.com:mvicha/MarcosFVilla.git cookbooks",
			"sudo chef-client -z -o 'recipe[nginx]'",
		]
	}
}
