## Deliverables
```
   +--|++++++++++|-----------------------------------------------------------------------------------+
   |  |  AWS     |                                                                                   |
   |  +----------+                                                                                   |
   |                                                                                                 |
   |                             +--|+++++|-------------------------------+                          |
   |                             |  |VPC2 |                               |                          |
   |                             |  +-----+                               |                          |
   |                             |   Instance name: auxischef             |                          |
   |                             |                                        |                          |
   |                             |   +|+++++++++++++++++++++++++++++++|+  |                          |
   |                             |   || private subnet                ||  |                          |
   |                             |   |+-------------------------------+|  |                          |
   |                             |   |  +---------------------------+  |  |                          |
   |                             |   |  | ec2 workstation prepared  |  |  |                          |
   |                             |   |  | with all necessary tools  |  |  |                          |
   |                             |   |  | to launch instances and   |  |  |                          |
   |                             |   |  | deploy nginx as requested |  |  |                          |
   |                             |   |  +---------------------------+  |  |                          |
   |                             |   +---------------------------------+  |                          |
   |                             |                                        |                          |
   |                             +------------------+---------------------+                          |
   |                                               / \                                               |
   |                                              /   \                                              |
   |   +--|+++++|-------------------------------+       +--|+++++|-------------------------------+   |
   |   |  |VPC1 |                               |       |  |VPC2 |                               |   |
   |   |  +-----+                               |       |  +-----+                               |   |
   |   |   Instance name: auxislb               |       |  Instance name: auxisws                |   |
   |   |                                        |       |                                        |   |
   |   |   +|+++++++++++++++++++++++++++++++|+  |       |   +|+++++++++++++++++++++++++++++++|+  |   |
   |   |   || public subnet                 ||  |       |   || private subnet                ||  |   |
   |   |   |+-------------------------------+|  |       |   |+-------------------------------+|  |   |
   |   |   |  +---------------------------+  |  |       |   |  +---------------------------+  |  |   |
   |   |   |  |                           |  |  |       |   |  |                           |  |  |   |
   |   |   |  |  ec2 instance acting as   |<-+--+-------+---+->|  ec2 instance running a   |  |  |   |
   |   |   |  |  a load balancer (nginx)  |  |  |       |   |  |  web server(nginx)        |  |  |   |
   |   |   |  |                           |  |  |       |   |  |                           |  |  |   |
   |   |   |  +---------------------------+  |  |       |   |  +---------------------------+  |  |   |
   |   |   +---------------------------------+  |       |   +---------------------------------+  |   |
   |   |                                        |       |                                        |   |
   |   +----------------------------------------+       +----------------------------------------+   |
   +-------------------------------------------------------------------------------------------------+

```

### Instructions

Requirements:
You only need to download Terraform for your current OS from https://www.terraform.io/downloads.html. This project provides the rest of the tools to achieve its objective.

Following are the steps to make this happen:

1) Obtain the repository:
  $ git clone git@gitlab.com:auxis/MarcosFVilla.git

2) Write the values of the provided AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY into MarcosFVilla/chef/terraform.tfvars

3) Create the environment variables, needed for the service to run the first time.
  $ export AWS_ACCESS_KEY_ID=""
  $ export AWS_SECRET_ACCESS_KEY=""

4) Copy the public key file you use for cloning the git repository into MarcosFVilla/chef/ssh/gitlab_id_rsa

5) Create a directory for ssh keys
  $ mkdir MarcosFVilla/chef/ssh

6) Copy the id_rsa file you received by email into MarcosFVilla/chef/ssh/id_rsa

7) Make sure the key files have the proper permissions
  $ chmod 400 MarcosFVilla/chef/ssh/gitlab_id_rsa
  $ chmod 400 MarcosFVilla/chef/ssh/id_rsa

8) Initialize Terraform and launch project
  $ cd MarcosFVilla/chef
  $ terraform init
  $ terraform apply
  Answer yes when asked

9) Wait for the process to finish. It takes around 12 minutes to complete

10) Open the URL shown in the output. You should get a webpage saying it is running on auxisws.

#### Example of WEB URL STRING TO LOOK FOR IN THE OUTPUT ####
aws_eip.auxischef-eip-instance (remote-exec):            "WEB URL": {
aws_eip.auxischef-eip-instance (remote-exec):         "sensitive": false,
aws_eip.auxischef-eip-instance (remote-exec):         "type": "string",
aws_eip.auxischef-eip-instance (remote-exec):         "value": "http://54.88.235.113/"
aws_eip.auxischef-eip-instance (remote-exec):            }
#############################################################


#### ROLLBACK ####
In case you want to roll back all the changes you will have to:

1) Obtain auxischef eip address:
  $ cd MarcosFVilla/chef
  $ terraform output
  Look for the value of Auxis_Chef_elastic_ip

2) SSH into auxischef
  $ ssh -i ssh/id_rsa admin@EIP

3) Inside auxischef run the following:
  $ cd chef-repo/nginx
  $ kitchen destroy

4) Logout of auxischef and destroy it
  $ terraform destroy
  Answer yes when asked
  In case you get any errors when destroying please try again

